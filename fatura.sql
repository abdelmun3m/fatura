create table  public.categories
(
    id        serial      not null
        constraint id_pk
            primary key,
    name      varchar(45) not null,
    parent_id integer
        constraint categories_fk
            references categories
);

alter table categories
    owner to postgres;

create table  public. products
(
    id          serial               not null
        constraint products_pk
            primary key,
    name        varchar(45)          not null,
    image_url   varchar(255),
    category_id integer              not null
        constraint category___fk
            references categories,
    featured    boolean default true not null
);

alter table products
    owner to postgres;

create unique index products_id_uindex
    on products (id);

create table  public.provider
(
    id   serial      not null
        constraint provider_pk
            primary key,
    name varchar(45) not null
);

alter table provider
    owner to postgres;

create unique index provider_id_uindex
    on provider (id);

create table  public.product_providers
(
    product_id  integer not null
        constraint product_providers_products_id_fk
            references products,
    provider_id integer not null
        constraint product_providers_provider_id_fk
            references provider,
    price       numeric not null,
    avilable    boolean default true,
    constraint product_providers_pk
        primary key (product_id, provider_id)
);

alter table product_providers
    owner to postgres;

