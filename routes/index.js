var express = require('express');
var router = express.Router();
var {PRODUCT_ROUTER} = require('../products')
router.use("/products", PRODUCT_ROUTER)

module.exports = router;
