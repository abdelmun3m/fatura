## to run docker-compose 
1. cd project dir 
2. please note that ports 3000 and 5432 should be avilable 
3. run docker-compose build
4. run docker-compose up
5. open http://localhost:3000/api/products/



## for backEnd 
1. node version 14.15.4
2. cd project dir
3. npm i
4. configure .env file to set required varables
5. npm start