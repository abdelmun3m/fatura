FROM node:14.15.4
WORKDIR /app
ADD package.json .
RUN npm install 
COPY . .
EXPOSE 80
CMD npm run prod

