
module.exports = (limit) => {
    return (req, res , next)=>{
        const { page } = req.query
        const currentPage = Number(page) || 1
        req.limit = limit
        req.offset = (currentPage - 1) * req.limit
        next()
    }
}