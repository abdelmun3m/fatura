const { Client } = require('pg')


const connectionParam = {
    user: process.env.DBuser,
    host: process.env.DBhost,
    database: process.env.DBdatabase,
    password: process.env.DBpassword,
    port: process.env.DBport,
  }

class DbProvider {

    async init() {
        this.client = new Client(
            connectionParam
        )
        await this.client.connect()
    }

    async execQuery(query, params){
        return this.client.query(query, params)
    }
}

module.exports = new DbProvider();