var express = require('express');
var router = express.Router();
const productsView = require('./products.view');
const paginfMidleware = require('../midlewares/paging.midleware');



router.route('/toggle/:product')
    .get(productsView.putToggleFeatured)

router.route('/:category?')
    .get(paginfMidleware(25),productsView.getCategoryList)



module.exports = router;
