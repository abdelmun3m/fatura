const { NotExtended } = require('http-errors');
const productController = require('./products.controller')

class ProductView {

   

    static async getCategoryList(req, res, next) {
        try {

            const { category } = req.params;
            const products = await productController.getProducts(category, req.offset , req.limit)
            res.send(products)
        } catch (err) {
            next(err.message)
        }

    }


    static async putToggleFeatured(req, res, next){
        try{
            const { product } = req.params;
            const products = await productController.togleFeatured(product)
            res.send(products)
        }catch(err){
            next(err.message)
        }
    
    }

}

module.exports = ProductView