const db = require('../Utils/DBProvider');

class ProductModel {

    static async getProducts(catergotry, offset, limit) {
        const sqlQuery = `
            select products.* , categories.name as category_name,
             provider.name as provider_name, provider.id as provider_id,
             pp.price
              from products
            left join categories on products.category_id = categories.id
            left join (
                select * from product_providers pp1
                where pp1.price = (select  min(price) from product_providers pp2 where pp1.product_id = pp2.product_id)
                ) as pp on pp.product_id = products.id
            left join provider on provider.id = pp.provider_id
            where ${ catergotry ? "products.category_id = $3 or categories.parent_id = $3 and" : '' } 
             featured = true
            limit $1 offset  $2`
        const sqlParams = [limit, offset]
        if (catergotry){
            sqlParams.push(catergotry)
        }
        const products = await db.execQuery(sqlQuery, sqlParams)
        return products.rows;
    }


    static async togleFeatured(product){
        const sqlQuery = 'update products set featured = not featured where id = $1'
        const result = await db.execQuery(sqlQuery, [product])
        return result
    }

}

module.exports = ProductModel;
