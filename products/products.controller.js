const productModel = require("./products.model")

class ProductController{

    static async getProducts(category, offset , limit){
        const products = await productModel.getProducts(category, offset , limit)
        return products;
    }

    static async togleFeatured(product){
        const products = await productModel.togleFeatured(product)
        return products;
    }

}

module.exports = ProductController